//要求：
//1. 定义一个抽象类，这个类实现某种运算
//2. 这个类的一个实例为特定的运算操作。运算操作需要两个对象
#if 0
class Operation
{
	public:
	//1. 构造函数
	Operation(double a, double b)
	:numA(0), numB(0)
	{
		numA = a;
		numB = b;
	}
	//2. 运算函数（对外的接口)
		virtual double GetResult()
		{
			double result = 0;
			return result;
		}
	
//	private:
		double numA;
		double numB;
};

class AddOperation:Operation
{
	public:

		AddOpeation(double a, double b):numA(0),numB(0)
		{
			numA = a;
			numB = b;
		}
		double GetResult() override
		{
			double result = 0;
			result = numA+numB;
			return result;
		}
};
#endif
class Operation
{
	public:
		double numA;
		double numB;

	//2. 运算函数（对外的接口)
		virtual double GetResult()
		{
			double result = 0;
			return result;
		}
	
};

class AddOperation:public Operation
{
	public:
		AddOperation(double a, double b)
		{
			numA = a;
			numB = b;
		}
		AddOperation()
		{
		}
		double GetResult() override
		{
			double result = 0;
			result = numA+numB;
			return result;
		}
};

class MinusOperation:public Operation
{
	public:
		double GetResult() override
		{
			double result = 0;
			result = numA-numB;
			return result;
		}
};


class MultiplyOperation:public Operation
{
	public:
		double GetResult() override
		{
			double result = 0;
			result = numA*numB;
			return result;
		}
};



class CCalculatorFactory
{
	public:
		static Operation *Create(char cOperator);
};


Operation * CCalculatorFactory::Create(char cOperator)
{
	Operation *oper;
	switch(cOperator)
	{
		case '+':oper = new AddOperation();
				break;
		case '-':oper = new MinusOperation();
				break;
		case '*':oper = new MultiplyOperation();
				break;
		default:oper = new AddOperation();
				break;
	}
	return oper;
}
