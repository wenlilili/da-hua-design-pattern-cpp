#include <iostream>
#include "factory.h"


using namespace std;

int main()
{
	cout << "hello" <<endl;
	//AddOperation addAlgo(10,20);
	//double res = addAlgo.GetResult();
	
	int a,b;
	cin >> a >>b;
#if 0 //采用工厂类
	cout << "please input operator"<<endl;
	char opechar;
	cin >> opechar;
	Operation *op =  CCalculatorFactory::Create(opechar);
#endif
#if 0 //不采用工厂类，采用指针
	Operation* op = new AddOperation();
	op->numA = a;
	op->numB = b;
	double res = op->GetResult();
	cout << "res ==" << res <<endl;
#endif
#if 0//不采用工厂类，采用对象
	//AddOperation op;
	MultiplyOperation op;
	op.numA = a;
	op.numB = b;
	double res = op.GetResult();
	cout << "res ==" << res <<endl;
#endif
	AddOperation op(10,15);
	double res = op.GetResult();
	cout << "res ==" << res <<endl;

	return 0;
}
